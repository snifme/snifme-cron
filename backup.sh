#! /bin/bash

echo "Hello world" >> /var/log/cron.log 2>&1

echo "Running rake data:backup_and_push on api1 container"
ssh -o "StrictHostKeyChecking no" \
api1 /bin/bash -c "ls && \
                   cd snifme-api && \
                   gem install bundle && \
                   bundle install && \
                   RAILS_ENV=production rake data:backup_and_push"



