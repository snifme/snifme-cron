#! /bin/bash

echo "Building docker image"
docker build -t mlennie/snifme-cron .

echo "stopping and removing container"
docker stop cron
docker rm cron

echo "running container"
docker run -d --net mynetwork --name cron mlennie/snifme-cron
