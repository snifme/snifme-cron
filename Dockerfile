FROM ubuntu:14.04

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev
RUN apt-get install -y vim ssh

# Add backup-cron file in the cron directory
ADD backup-cron /etc/cron.d/backup-cron

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/backup-cron

# add SSH file
RUN mkdir -p /root/.ssh
ADD id_rsa /root/.ssh/id_rsa
RUN chmod 700 /root/.ssh/id_rsa
RUN echo "Host github.com\n\tStrictHostKeyChecking no\n" >> /root/.ssh/config
RUN echo "    IdentityFile ~/.ssh/id_rsa" >> /etc/ssh/ssh_config

# Create the log file to be able to run tail
RUN touch /var/log/cron.log

ADD backup.sh backup.sh
# set backup script to executable
RUN /bin/chmod +x backup.sh
RUN chmod +x backup.sh

# Run the command on container startup
CMD cron && tail -f /var/log/cron.log

# docker build -t mlennie/snifme-cron .
# docker run -d --name cron mlennie/snifme-cron

#echo "Running rake data:backup_and_push on api1 container"
#ssh -o "StrictHostKeyChecking no" \
#api1 /bin/bash -c "ls && \
#                   cd snifme-api && \
#                   gem install bundle && \
#                   bundle install && \
#                   RAILS_ENV=production rake data:backup_and_push"
#
